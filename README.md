bml-fuzz
========

A simple app that tests nall's BML parsing,
for use with the [American Fuzzy Lop][afl] fuzzer.

Assuming you have `afl-fuzz` installed,
and all the usual compiler machinery,
you should be able to run:

    make test

...and the fuzzee binary will be built
and run inside the fuzzer.

[afl]: http://lcamtuf.coredump.cx/afl/
