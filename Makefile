build := release
compiler := afl-g++

include nall/GNUmakefile

link := -I.

fuzzee: fuzzee.cpp
	$(compiler) -o fuzzee $(link) $(flags) fuzzee.cpp

test: fuzzee
	afl-fuzz -i tests -o findings -- ./fuzzee @@

crashmin: fuzzee
	mkdir -p crashes-minimized
	for name in findings/crashes/id*; do num=$$(echo "$$name" | sed -Ee 's/.*id:([0-9]+),.*/\1/'); afl-tmin -i "$$name" -o crashes-minimized/$$num.bml -- ./fuzzee @@; done

clean:
	rm fuzzee

.DUMMY: all crashmin
