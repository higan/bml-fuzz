#include <stdio.h>
#include <stdlib.h>
#include <nall/nall.hpp>

int main(int argc, char *argv[]) {
    nall::string filename(argv[1]);

    // Round-trip through BML to produce normalized output.
    auto input = nall::string::read(filename);
    auto document = nall::BML::unserialize(input);
    auto output1 = nall::BML::serialize(document);

    nall::print(stderr, "After normalization:\n", output1, "\n");

    // Send it through again.
    document = nall::BML::unserialize(output1);
    auto output2 = nall::BML::serialize(document);

    nall::print(stderr, "After round-trip:\n");
    nall::print(output2);

    if (output1 != output2) {
	nall::print(stderr, "Round-trip error!\n");
	abort();
    }

    return 0;
}
